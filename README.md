# RubixCubeSolver

This is a python script I developed that will solve a rubix cube from any 
unsolved position. At the current time, the solver only implements beginner 
solving algorithms. In future I hope to teach myself the advanced solving 
algorithms and implement them into the script.