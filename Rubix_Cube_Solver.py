#!/usr/bin/env python
# coding: utf-8

# In[1]:


import random as rand
import numpy as np
class RubixCube:
    def __init__(self, WhiteSide, OrangeSide, BlueSide, RedSide, GreenSide, YellowSide):
        '''Initialisation method for a rubix cube, given its sides.'''
        
        #Checks if the centre of each side is the colour of that side
        if WhiteSide[1][1] != 'W' or OrangeSide[1][1] != 'O' or BlueSide[1][1] != 'B' or RedSide[1][1] != 'R' or GreenSide[1][1] != 'G' or YellowSide[1][1] != 'Y':
            print('Invalid cube')
            return
        
        #Checks if the number of coloured squares add up to the correct amount
        numWhite, numOrange, numBlue, numRed, numGreen, numYellow = 0, 0, 0, 0, 0, 0
        for side in [WhiteSide, OrangeSide, BlueSide, RedSide, GreenSide, YellowSide]:
            for row in side:
                for square in row:
                    if square == 'W':
                        numWhite += 1
                    elif square == 'O':
                        numOrange += 1
                    elif square == 'B':
                        numBlue += 1
                    elif square == 'R':
                        numRed += 1
                    elif square == 'G':
                        numGreen += 1
                    elif square == 'Y':
                        numYellow += 1
        if numWhite != 9 or numOrange != 9 or numBlue != 9 or numRed != 9 or numGreen != 9 or numYellow != 9:
            print('Invalid cube')
            return
                
        #Note to self: add a section to check for any invalid corner of side pieces e.g. a corner with white and yellow on it
        self.moves = ['up', 'up2', 'up3', 'down', 'down2', 'down3', 'front', 'front2', 'front3', 'back', 'back2', 'back3', 'left', 'left2', 'left3', 'right', 'right2', 'right3']
        self.moveHistory = []
        self.cornerList = []
        self.WhiteSide = np.array(WhiteSide)
        self.OrangeSide = np.array(OrangeSide)
        self.BlueSide = np.array(BlueSide)
        self.RedSide = np.array(RedSide)
        self.GreenSide = np.array(GreenSide)
        self.YellowSide = np.array(YellowSide)
        
    def printCube(self):
        '''Method for printing the cube in a readable manner.'''
        #print(self.moveHistory)
        print(self.WhiteSide)
        print(self.OrangeSide)
        print(self.BlueSide)
        print(self.RedSide)
        print(self.GreenSide)
        print(self.YellowSide)
        
    def printMH(self):
        '''Method for printing every move that has been done to the cube.'''
        print(self.moveHistory)
        
    def clearMH(self):
        self.moveHistory = []
        
    def up(self):
        '''Method that preforms the up move on the cube, assuming the cube is viewed from the white face.'''
        WhiteTopRow, BlueTopRow = tuple(self.WhiteSide[0]), tuple(self.BlueSide[0])
        YellowTopRow, GreenTopRow = tuple(self.YellowSide[0]), tuple(self.GreenSide[0])
        
        WhiteTopRow, BlueTopRow, YellowTopRow, GreenTopRow = BlueTopRow, YellowTopRow, GreenTopRow, WhiteTopRow
        
        self.WhiteSide[0], self.BlueSide[0] = WhiteTopRow, BlueTopRow
        self.YellowSide[0], self.GreenSide[0] = YellowTopRow, GreenTopRow
        
        self.OrangeSide = np.rot90(self.OrangeSide, 3)
        
    def down(self):
        '''Method that preforms the down move on the cube, assuming the cube is viewed from the white face.'''
        WhiteBotRow, BlueBotRow = tuple(self.WhiteSide[2]), tuple(self.BlueSide[2])
        YellowBotRow, GreenBotRow = tuple(self.YellowSide[2]), tuple(self.GreenSide[2])
        
        WhiteBotRow, BlueBotRow, YellowBotRow, GreenBotRow = GreenBotRow, WhiteBotRow, BlueBotRow, YellowBotRow
        
        self.WhiteSide[2], self.BlueSide[2] = WhiteBotRow, BlueBotRow
        self.YellowSide[2], self.GreenSide[2] = YellowBotRow, GreenBotRow
        
        self.RedSide = np.rot90(self.RedSide, 3)
        
    def front(self):
        '''Method that preforms the front move on the cube, assuming the cube is viewed from the white face.'''
        OrangeFroRow, BlueFroRow = tuple(self.OrangeSide[2]), (self.BlueSide[0][0], self.BlueSide[1][0], self.BlueSide[2][0])
        RedFroRow, GreenFroRow = tuple(self.RedSide[2]), (self.GreenSide[2][2], self.GreenSide[1][2], self.GreenSide[0][2])
        
        OrangeFroRow, BlueFroRow, RedFroRow, GreenFroRow = GreenFroRow, OrangeFroRow, BlueFroRow, RedFroRow
        
        self.OrangeSide[2], self.RedSide[2] = OrangeFroRow, RedFroRow
        self.BlueSide[0][0], self.BlueSide[1][0], self.BlueSide[2][0] = BlueFroRow[0], BlueFroRow[1], BlueFroRow[2]
        self.GreenSide[2][2], self.GreenSide[1][2], self.GreenSide[0][2] = GreenFroRow[0], GreenFroRow[1], GreenFroRow[2]
        
        self.WhiteSide = np.rot90(self.WhiteSide, 3)
        
    def back(self):
        '''Method that preforms the back move on the cube, assuming the cube is viewed from the white face.'''
        OrangeFroRow, BlueFroRow = tuple(self.OrangeSide[0]), (self.BlueSide[0][2], self.BlueSide[1][2], self.BlueSide[2][2])
        RedFroRow, GreenFroRow = tuple(self.RedSide[0]), (self.GreenSide[2][0], self.GreenSide[1][0], self.GreenSide[0][0])
        
        OrangeFroRow, BlueFroRow, RedFroRow, GreenFroRow = BlueFroRow, RedFroRow, GreenFroRow, OrangeFroRow
        
        self.OrangeSide[0], self.RedSide[0] = OrangeFroRow, RedFroRow
        self.BlueSide[0][2], self.BlueSide[1][2], self.BlueSide[2][2] = BlueFroRow[0], BlueFroRow[1], BlueFroRow[2]
        self.GreenSide[2][0], self.GreenSide[1][0], self.GreenSide[0][0] = GreenFroRow[0], GreenFroRow[1], GreenFroRow[2]
        
        self.YellowSide = np.rot90(self.YellowSide, 3)
        
    def left(self):
        '''Method that preforms the left move on the cube, assuming the cube is viewed from the white face.'''
        WhiteLeftCol = (self.WhiteSide[0][0], self.WhiteSide[1][0], self.WhiteSide[2][0])
        RedLeftCol = (self.RedSide[2][2], self.RedSide[1][2], self.RedSide[0][2])
        YellowLeftCol = (self.YellowSide[2][2], self.YellowSide[1][2], self.YellowSide[0][2])
        OrangeLeftCol = (self.OrangeSide[0][0], self.OrangeSide[1][0], self.OrangeSide[2][0])
        
        WhiteLeftCol, RedLeftCol, YellowLeftCol, OrangeLeftCol = OrangeLeftCol, WhiteLeftCol, RedLeftCol, YellowLeftCol
        
        self.WhiteSide[0][0], self.WhiteSide[1][0], self.WhiteSide[2][0] = WhiteLeftCol[0], WhiteLeftCol[1], WhiteLeftCol[2]
        self.RedSide[2][2], self.RedSide[1][2], self.RedSide[0][2] = RedLeftCol[0], RedLeftCol[1], RedLeftCol[2]
        self.YellowSide[2][2], self.YellowSide[1][2], self.YellowSide[0][2] = YellowLeftCol[0], YellowLeftCol[1], YellowLeftCol[2]
        self.OrangeSide[0][0], self.OrangeSide[1][0], self.OrangeSide[2][0] = OrangeLeftCol[0], OrangeLeftCol[1], OrangeLeftCol[2]
        
        self.GreenSide = np.rot90(self.GreenSide, 3)
        
    def right(self):
        '''Method that preforms the right move on the cube, assuming the cube is viewed from the white face.'''
        WhiteLeftCol = (self.WhiteSide[0][2], self.WhiteSide[1][2], self.WhiteSide[2][2])
        RedLeftCol = (self.RedSide[2][0], self.RedSide[1][0], self.RedSide[0][0])
        YellowLeftCol = (self.YellowSide[2][0], self.YellowSide[1][0], self.YellowSide[0][0])
        OrangeLeftCol = (self.OrangeSide[0][2], self.OrangeSide[1][2], self.OrangeSide[2][2])
        
        WhiteLeftCol, RedLeftCol, YellowLeftCol, OrangeLeftCol = RedLeftCol, YellowLeftCol, OrangeLeftCol, WhiteLeftCol
        
        self.WhiteSide[0][2], self.WhiteSide[1][2], self.WhiteSide[2][2] = WhiteLeftCol[0], WhiteLeftCol[1], WhiteLeftCol[2]
        self.RedSide[2][0], self.RedSide[1][0], self.RedSide[0][0] = RedLeftCol[0], RedLeftCol[1], RedLeftCol[2]
        self.YellowSide[2][0], self.YellowSide[1][0], self.YellowSide[0][0] = YellowLeftCol[0], YellowLeftCol[1], YellowLeftCol[2]
        self.OrangeSide[0][2], self.OrangeSide[1][2], self.OrangeSide[2][2] = OrangeLeftCol[0], OrangeLeftCol[1], OrangeLeftCol[2]
        
        self.BlueSide = np.rot90(self.BlueSide, 3)
        
    def performMove(self, move):
        '''Method used for performing a move.'''
        if move == 'up':
            self.up()
        elif move == 'up2':
            self.up()
            self.up()
        elif move == 'up3':
            self.up()
            self.up()
            self.up()
        elif move == 'down':
            self.down()
        elif move == 'down2':
            self.down()
            self.down()
        elif move == 'down3':
            self.down()
            self.down()
            self.down()
        elif move == 'front':
            self.front()
        elif move == 'front2':
            self.front()
            self.front()
        elif move == 'front3':
            self.front()
            self.front()
            self.front()
        elif move == 'back':
            self.back()
        elif move == 'back2':
            self.back()
            self.back()
        elif move == 'back3':
            self.back()
            self.back()
            self.back()
        elif move == 'left':
            self.left()
        elif move == 'left2':
            self.left()
            self.left()
        elif move == 'left3':
            self.left()
            self.left()
            self.left()
        elif move == 'right':
            self.right()
        elif move == 'right2':
            self.right()
            self.right()
        elif move == 'right3':
            self.right()
            self.right()
            self.right()
        else:
            print(move)
            print('Invalid Move')
        self.moveHistory.append(move)
        self.updateCL()

            
    def performMoves(self, moves):
        for move in moves:
            self.performMove(move)
                
    def isSolved(self):
        '''Method that checks whether the cube is solved.'''
        for row in self.WhiteSide:
            for square in row:
                if square != 'W':
                    return False
        for row in self.OrangeSide:
            for square in row:
                if square != 'O':
                    return False
        for row in self.BlueSide:
            for square in row:
                if square != 'B':
                    return False
        for row in self.RedSide:
            for square in row:
                if square != 'R':
                    return False
        for row in self.GreenSide:
            for square in row:
                if square != 'G':
                    return False
        for row in self.YellowSide:
            for square in row:
                if square != 'Y':
                    return False
        return True
    
    def randSolve(self):
        '''Method that will randomly do moves until it has solved the cube (just made for fun).'''
        for i in range(100000):
            num = rand.randrange(0, 18, 1)
            move = self.moves[num]
            self.performMove(move)
            if(self.isSolved()):
                self.printCube()
                return True
        self.printCube()
        return False
    
    def mix(self):
        '''Method that will mix up the cube randomly.'''
        for i in range(100):
            num = rand.randrange(0, 18, 1)
            move = self.moves[num]
            self.performMove(move)
        self.printCube()
        
    def isLayerOneSolved(self):
        '''Method that will check if the first layer of the cube has been solved.'''
        for row in self.YellowSide:
            for square in row:
                if square != 'Y':
                    return False
        for square in self.RedSide[0]:
            if square != 'R':
                return False
        for square in self.OrangeSide[0]:
            if square != 'O':
                return False
        for i in range(3):
            if self.BlueSide[i][2] != 'B' or self.GreenSide[i][0] != 'G':
                return False
        return True
    
    def isLayerTwoSolved(self):
        '''Method that will check if the second layer of the cube has been solved.'''
        if self.OrangeSide[1][0] != 'O' or self.OrangeSide[1][2] != 'O' or self.BlueSide[0][1] != 'B' or self.BlueSide[2][1] != 'B':
            return False
        if self.RedSide[1][0] != 'R' or self.RedSide[1][2] != 'R' or self.GreenSide[0][1] != 'G' or self.GreenSide[2][1] != 'G':
            return False
        return True
    
    def hasWhiteThing(self):
        '''Method that checks if the correct pattern is present on the white side.'''
        if self.WhiteSide[2][1] == 'W' and self.WhiteSide[1][2] == 'W':
            return True
        if self.WhiteSide[1][2] == 'W' and self.WhiteSide[0][1] == 'W':
            self.performMove('front')
            return True
        if self.WhiteSide[1][0] == 'W' and self.WhiteSide[2][1] == 'W':
            self.performMove('front2')
            return True
        if self.WhiteSide[0][1] == 'W' and self.WhiteSide[1][0] == 'W':
            self.performMove('front3')
            return True
        return False
    
    def isWhiteCrossSolved(self):
        '''Method to check if the white face has been solved.'''
        for i in range(3):
            if self.WhiteSide[i][1] != 'W':
                return False
        if self.WhiteSide[1][0] != 'W' or self.WhiteSide[1][2] != 'W':
            return False
        return True
    
    def isWhiteCornersSolved(self):
        cornerOne = (self.cornerList[0][0], self.cornerList[0][1], self.cornerList[0][2])
        cornerTwo = (self.cornerList[1][0], self.cornerList[1][1], self.cornerList[1][2])
        cornerThree = (self.cornerList[2][0], self.cornerList[2][1], self.cornerList[2][2])
        cornerFour = (self.cornerList[3][0], self.cornerList[3][1], self.cornerList[3][2])
        cornersSolved = 0
        if sorted(['W','O','G']) == sorted(cornerOne):
            cornersSolved += 1
        if sorted(['W','O','B']) == sorted(cornerTwo):
            cornersSolved += 1
        if sorted(['W','R','G']) == sorted(cornerFour):
            cornersSolved += 1
        if sorted(['W','R','B']) == sorted(cornerThree):
            cornersSolved += 1
        return cornersSolved
    
    def whiteCornersSolvedList(self):
        cornerOne = (self.cornerList[0][0], self.cornerList[0][1], self.cornerList[0][2])
        cornerTwo = (self.cornerList[1][0], self.cornerList[1][1], self.cornerList[1][2])
        cornerThree = (self.cornerList[2][0], self.cornerList[2][1], self.cornerList[2][2])
        cornerFour = (self.cornerList[3][0], self.cornerList[3][1], self.cornerList[3][2])
        cornersSolved = []
        if sorted(['W','O','G']) == sorted(cornerOne):
            cornersSolved.append(True)
        else:
            cornersSolved.append(False)
        if sorted(['W','O','B']) == sorted(cornerTwo):
            cornersSolved.append(True)
        else:
            cornersSolved.append(False)
        if sorted(['W','R','G']) == sorted(cornerFour):
            cornersSolved.append(True)
        else:
            cornersSolved.append(False)
        if sorted(['W','R','B']) == sorted(cornerThree):
            cornersSolved.append(True)
        else:
            cornersSolved.append(False)
        return cornersSolved
    
    def isWhiteFaceSolved(self):
        '''Method to check if the white face has been solved.'''
        for i in range(3):
            for x in range(3):
                if self.WhiteSide[i][x] != 'W':
                    return False
        return True
        
    def updateCL(self):
        self.cornerList = []
        self.cornerList.append([self.WhiteSide[0][0], self.OrangeSide[2][0], self.GreenSide[0][2]])
        self.cornerList.append([self.WhiteSide[0][2], self.OrangeSide[2][2], self.BlueSide[0][0]])
        self.cornerList.append([self.WhiteSide[2][2], self.RedSide[2][0], self.BlueSide[2][0]])
        self.cornerList.append([self.WhiteSide[2][0], self.RedSide[2][2], self.GreenSide[2][2]])
        
        self.cornerList.append([self.YellowSide[0][0], self.OrangeSide[0][2], self.BlueSide[0][2]])
        self.cornerList.append([self.YellowSide[0][2], self.OrangeSide[0][0], self.GreenSide[0][0]])
        self.cornerList.append([self.YellowSide[2][2], self.RedSide[0][2], self.GreenSide[2][0]])
        self.cornerList.append([self.YellowSide[2][0], self.RedSide[0][0], self.BlueSide[2][2]])
        
        hasWhite = 0
        hasYellow = 0
        hasRed = 0
        hasOrange = 0
        hasBlue = 0
        hasGreen = 0
        
        for corner in self.cornerList:
            for square in corner:
                if square == 'W':
                    hasWhite += 1
                elif square == 'O':
                    hasOrange += 1
                elif square == 'B':
                    hasBlue += 1
                elif square == 'R':
                    hasRed += 1
                elif square == 'G':
                    hasGreen += 1
                elif square == 'Y':
                    hasYellow += 1
            if hasWhite and hasYellow:
                return False
            elif hasOrange and hasRed:
                return False
            elif hasBlue and hasGreen:
                return False
            elif hasWhite == 2 or hasYellow == 2 or hasRed == 2 or hasOrange == 2 or hasBlue == 2 or hasGreen == 2:
                return False
            hasWhite = 0
            hasYellow = 0
            hasRed = 0
            hasOrange = 0
            hasBlue = 0
            hasGreen = 0
        
    def printCL(self):
        print(self.cornerList)
        
    def printSL(self):
        sideList = []
        sideList.append([self.OrangeSide[1][0], self.GreenSide[0][1]])
        sideList.append([self.OrangeSide[1][2], self.BlueSide[0][1]])
        sideList.append([self.RedSide[1][2], self.GreenSide[2][1]])
        sideList.append([self.RedSide[1][0], self.BlueSide[2][1]])
        
        print(sideList)
    
    def rightAlgo(self, face, num):
        '''Method for performing the right algorithm, with respect to a face from the cube.'''
        for i in range(num):
            if face == 'R':
                self.performMoves(['right', 'front', 'right3', 'front3'])
            elif face == 'B':
                self.performMoves(['up', 'front', 'up3', 'front3'])
            elif face == 'O':
                self.performMoves(['left', 'front', 'left3', 'front3'])
            elif face == 'G':
                self.performMoves(['down', 'front', 'down3', 'front3'])
            else:
                print(face)
                print("Invalid face")
                
    def rightAlgoPrime(self):
        '''Method for performing a right algorithm for solving the white corners.'''
        self.performMoves(['right', 'back', 'right3', 'back3'])
                
    def leftAlgo(self, face, num):
        '''Method for performing the left algorithm, with respect to a face from the cube.'''
        for i in range(num):
            if face == 'R':
                self.performMoves(['left3', 'front3', 'left', 'front'])
            elif face == 'B':
                self.performMoves(['down3', 'front3', 'down', 'front'])
            elif face == 'O':
                self.performMoves(['right3', 'front3', 'right', 'front'])
            elif face == 'G':
                self.performMoves(['up3', 'front3', 'up', 'front'])
            else:
                print(face)
                print("Invalid face")
                
    def cornerSwap(self, face):
        if face == 'R':
            self.rightAlgo('R',3)
            self.leftAlgo('B',3)
            self.performMove('front3')
        elif face == 'B':
            self.rightAlgo('B',3)
            self.leftAlgo('O',3)
            self.performMove('front3')
        elif face == 'O':
            self.rightAlgo('O',3)
            self.leftAlgo('G',3)
            self.performMove('front3')
        elif face == 'G':
            self.rightAlgo('G',3)
            self.leftAlgo('R',3)
            self.performMove('front3')
        else:
            print(face)
            print("Invalid face")
                
    def daisy(self):
        '''Method that gets an unsolved rubix cube into the daisy state.'''
        #Check if the cube is already in the daisy state
        while(self.WhiteSide[0][1] != 'Y' or self.WhiteSide[1][0] != 'Y' or self.WhiteSide[1][2] != 'Y' or self.WhiteSide[2][1] != 'Y'):
            #Checks for all the single move soultions
            if self.WhiteSide[0][1] != 'Y':
                if self.BlueSide[0][1] == 'Y':
                    self.performMove('up')
                elif self.YellowSide[0][1] == 'Y':
                    self.performMove('up2')
                elif self.GreenSide[0][1] == 'Y':
                    self.performMove('up3')
            if self.WhiteSide[1][0] != 'Y':
                if self.OrangeSide[1][0] == 'Y':
                    self.performMove('left')
                elif self.YellowSide[1][2] == 'Y':
                    self.performMove('left2')
                elif self.RedSide[1][0] == 'Y':
                    self.performMove('left3')
            if self.WhiteSide[1][2] != 'Y':
                if self.RedSide[1][2] == 'Y':
                    self.performMove('right')
                elif self.YellowSide[1][0] == 'Y':
                    self.performMove('right2')
                elif self.OrangeSide[1][2] == 'Y':
                    self.performMove('right3')
            if self.WhiteSide[2][1] != 'Y':
                if self.GreenSide[2][1] == 'Y':
                    self.performMove('down')
                elif self.YellowSide[2][1] == 'Y':
                    self.performMove('down2')
                elif self.BlueSide[2][1] == 'Y':
                    self.performMove('down3')
                    
            if self.WhiteSide[0][1] != 'Y':
                self.performMove('up')
            elif self.WhiteSide[1][0] != 'Y':
                self.performMove('left')
            elif self.WhiteSide[1][2] != 'Y':
                self.performMove('right')
            elif self.WhiteSide[2][1] != 'Y':
                self.performMove('down')
            self.performMove('front')
        print('Daisy made')
        
    def swap(self):
        while (self.WhiteSide[0][1] == 'Y' or self.WhiteSide[1][0] == 'Y' or self.WhiteSide[1][2] == 'Y' or self.WhiteSide[2][1] == 'Y'):
            if self.OrangeSide[2][1] == 'O':
                self.performMove('up2')
            if self.BlueSide[1][0] == 'B':
                self.performMove('right2')
            if self.RedSide[2][1] == 'R':
                self.performMove('down2')
            if self.GreenSide[1][2] == 'G':
                self.performMove('left2')
            self.performMove('front')
        print('Swap complete')
        
    def layerOne(self):
        while(not self.isLayerOneSolved()):
            frontCounter = 0
            while (frontCounter != 4):
                if self.cornerList[4] != ['Y','O','B']:
                    if self.cornerList[1] == ['O','Y','B']:
                        self.rightAlgo('B', 1)
                    elif self.cornerList[4] == ['B','Y', 'O']:
                        self.rightAlgo('B', 2)
                    elif self.cornerList[1] == ['Y','B','O']:
                        self.rightAlgo('B', 3)
                    elif self.cornerList[4] == ['O','B','Y']:
                        self.rightAlgo('B', 4)
                    elif self.cornerList[1] == ['B','O','Y']:
                        self.rightAlgo('B', 5)
                if self.cornerList[5] != ['Y','O','G']:
                    if self.cornerList[0] == ['G','O','Y']:
                        self.rightAlgo('O', 1)
                    elif self.cornerList[5] == ['O','G','Y']:
                        self.rightAlgo('O', 2)
                    elif self.cornerList[0] == ['Y','G','O']:
                        self.rightAlgo('O', 3)
                    elif self.cornerList[5] == ['G','Y', 'O']:
                        self.rightAlgo('O', 4)
                    elif self.cornerList[0] == ['O','Y','G']:
                        self.rightAlgo('O', 5)
                if self.cornerList[7] != ['Y', 'R', 'B']:
                    if self.cornerList[2] == ['B','R','Y']:
                        self.rightAlgo('R', 1)
                    elif self.cornerList[7] == ['R','B','Y']:
                        self.rightAlgo('R', 2)
                    elif self.cornerList[2] == ['Y','B','R']:
                        self.rightAlgo('R', 3)
                    elif self.cornerList[7] == ['B','Y', 'R']:
                        self.rightAlgo('R', 4)
                    elif self.cornerList[2] == ['R','Y','B']:
                        self.rightAlgo('R', 5)
                if self.cornerList[6] != ['Y', 'R', 'G']:
                    if self.cornerList[3] == ['R','Y','G']:
                        self.rightAlgo('G', 1)
                    elif self.cornerList[6] == ['G','Y','R']:
                        self.rightAlgo('G', 2)
                    elif self.cornerList[3] == ['Y','G','R']:
                        self.rightAlgo('G', 3)
                    elif self.cornerList[6] == ['R','G', 'Y']:
                        self.rightAlgo('G', 4)
                    elif self.cornerList[3] == ['G','R','Y']:
                        self.rightAlgo('G', 5)
                self.performMove('front')
                frontCounter += 1
                
            if self.cornerList[4] != ['Y','O','B']:
                self.rightAlgo('B', 1)
            elif self.cornerList[5] != ['Y','O','G']:
                self.rightAlgo('O', 1)
            elif self.cornerList[6] != ['Y', 'R', 'G']:
                self.rightAlgo('G', 1)
            elif self.cornerList[7] != ['Y', 'R', 'B']:
                self.rightAlgo('R', 1)
        print('Corners complete')
        
    def layerTwo(self):
        while(not self.isLayerTwoSolved()):
            frontCounter = 0
            while(frontCounter != 4):
                if self.OrangeSide[1][0] != 'O' or self.GreenSide[0][1] != 'G':
                    if self.OrangeSide[2][1] == 'O' and self.WhiteSide[0][1] == 'G':
                        self.performMove('front')
                        self.rightAlgo('O', 1)
                        self.leftAlgo('G', 1)
                    elif self.GreenSide[1][2] == 'G' and self.WhiteSide[1][0] == 'W':
                        self.performMove('front3')
                        self.leftAlgo('G', 1)
                        self.rightAlgo('O', 1)
                if self.OrangeSide[1][2] != 'O' or self.BlueSide[0][1] != 'B':
                    if self.OrangeSide[2][1] == 'O' and self.WhiteSide[0][1] == 'B':
                        self.performMove('front3')
                        self.leftAlgo('O', 1)
                        self.rightAlgo('B', 1)
                    elif self.BlueSide[1][0] == 'B' and self.WhiteSide[1][2] == 'O':
                        self.performMove('front')
                        self.rightAlgo('B', 1)
                        self.leftAlgo('O', 1)
                if self.RedSide[1][2] != 'R' or self.GreenSide[2][1] != 'G':
                    if self.RedSide[2][1] == 'R' and self.WhiteSide[2][1] == 'G':
                        self.performMove('front3')
                        self.leftAlgo('R', 1)
                        self.rightAlgo('G', 1)
                    elif self.GreenSide[1][2] == 'G' and self.WhiteSide[1][0] == 'R':
                        self.performMove('front')
                        self.rightAlgo('G', 1)
                        self.leftAlgo('R', 1)
                if self.RedSide[1][0] != 'R' or self.BlueSide[2][1] != 'B':
                    if self.RedSide[2][1] == 'R' and self.WhiteSide[0][1] == 'B':
                        self.performMove('front')
                        self.rightAlgo('R', 1)
                        self.leftAlgo('B', 1)
                    elif self.BlueSide[1][0] == 'B' and self.WhiteSide[1][2] == 'R':
                        self.performMove('front3')
                        self.leftAlgo('B', 1)
                        self.rightAlgo('R', 1)
                self.performMove('front')
                frontCounter += 1
            
            if self.OrangeSide[1][0] != 'O' or self.GreenSide[0][1] != 'G':
                self.leftAlgo('G', 1)
                self.rightAlgo('O', 1)
            elif self.OrangeSide[1][2] != 'O' or self.BlueSide[0][1] != 'B':
                self.rightAlgo('B', 1)
                self.leftAlgo('O', 1)
            elif self.RedSide[1][2] != 'R' or self.GreenSide[2][1] != 'G':
                self.rightAlgo('G', 1)
                self.leftAlgo('R', 1)
            elif self.RedSide[1][0] != 'R' or self.BlueSide[2][1] != 'B':
                self.leftAlgo('B', 1)
                self.rightAlgo('R', 1)
                
        print('Layer two complete')
        
    def whiteFace(self):
        while(not self.isWhiteCrossSolved()):
            self.performMoves(['up','front','left','front3','left3','up3'])
            if self.hasWhiteThing():
                self.performMoves(['up','front','left','front3','left3','up3'])
                
        print('White face solved')
        
    def whiteCorners(self):
        while (self.isWhiteCornersSolved() != 4):
            while (self.isWhiteCornersSolved() < 2):
                self.performMove('front')
                if (self.whiteCornersSolvedList() == [True,False,True,False] or self.whiteCornersSolvedList() == [False,True,False,True]):
                    self.cornerSwap('O')
            if (self.whiteCornersSolvedList() == [True,True,False,False]):
                self.cornerSwap('G')
            elif (self.whiteCornersSolvedList() == [True,False,False,True]):
                self.cornerSwap('R')
            elif (self.whiteCornersSolvedList() == [False,False,True,True]):
                self.cornerSwap('B')
            elif (self.whiteCornersSolvedList() == [False,True,True,False]):
                self.cornerSwap('O')
        print('White corners solved')
        
    def correctWhiteCorners(self):
        while (self.cornerList[1][0] != 'W'):
            self.rightAlgoPrime()
        self.performMove('front')
        while (self.cornerList[1][0] != 'W'):
            self.rightAlgoPrime()
        self.performMove('front')
        while (self.cornerList[1][0] != 'W'):
            self.rightAlgoPrime()
        self.performMove('front')
        while (self.cornerList[1][0] != 'W'):
            self.rightAlgoPrime()
        self.performMove('front')
        print('White corners facing the right way')
            
        


# In[2]:


SolvedRubixCube = RubixCube([['W','W','W'],['W','W','W'],['W','W','W']],[['O','O','O'],['O','O','O'],['O','O','O']],[['B','B','B'],['B','B','B'],['B','B','B']],[['R','R','R'],['R','R','R'],['R','R','R']],[['G','G','G'],['G','G','G'],['G','G','G']],[['Y','Y','Y'],['Y','Y','Y'],['Y','Y','Y']])
